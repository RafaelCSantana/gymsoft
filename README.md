# Gymsoft

Application focused in Gym

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

You need to install the follow software list

**Technologies**

- JDK 1.8+
- Maven
- Node
- Vue.js
- Spring Boot

### Installing

A step by step series of commands that tell you how to get a development env running

Inside the root directory in project

```
$ mvn clean install 
```

####Backend

Run Spring Boot App
```
$ mvn --projects backend spring-boot:run
```

####Frontend

Go to frontend folder project
```
$ cd frontend 
```

Run Vue App
```
$ npm run serve
```